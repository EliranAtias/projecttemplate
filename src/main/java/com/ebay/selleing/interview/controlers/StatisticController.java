package com.ebay.selleing.interview.controlers;

import com.ebay.selleing.interview.pojos.resoult.AccountManagerStatistics;
import com.ebay.selleing.interview.services.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
public class StatisticController {

    @Autowired
    AppService appService;

    @RequestMapping("/statistic/{accountManager}")
    public AccountManagerStatistics getAccountManagerStatistics(@PathVariable String accountManager) {
        return appService.getAccountManagerStatistics(accountManager);
    }
}
