package com.ebay.selleing.interview.pojos.mocks;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContextData{
  private Boolean contextAsSite;
  private Boolean contextAsSeller;
  private Condition contextCondition;

  public Boolean getContextAsSite() {
    return contextAsSite;
  }

  public void setContextAsSite(Boolean contextAsSite) {
    this.contextAsSite = contextAsSite;
  }

  public Boolean getContextAsSeller() {
    return contextAsSeller;
  }

  public void setContextAsSeller(Boolean contextAsSeller) {
    this.contextAsSeller = contextAsSeller;
  }

  public Condition getContextCondition() {
    return contextCondition;
  }

  public void setContextCondition(Condition contextCondition) {
    this.contextCondition = contextCondition;
  }
}
