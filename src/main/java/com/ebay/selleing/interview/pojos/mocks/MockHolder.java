package com.ebay.selleing.interview.pojos.mocks;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MockHolder{
  private List<Document> docList;


  public void setDocList(List<Document> docList) {
    this.docList = docList;
  }

  public List<Document> getDocList() {
    return docList;
  }
}
