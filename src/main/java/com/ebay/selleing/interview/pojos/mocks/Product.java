package com.ebay.selleing.interview.pojos.mocks;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product{
  private String epid;
  private int totalActiveQuantity;
  private int totalActiveSellers;
  private ContextData contextData;

  public String getEpid() {
    return epid;
  }

  public void setEpid(String epid) {
    this.epid = epid;
  }

  public int getTotalActiveQuantity() {
    return totalActiveQuantity;
  }

  public void setTotalActiveQuantity(int totalActiveQuantity) {
    this.totalActiveQuantity = totalActiveQuantity;
  }

  public int getTotalActiveSellers() {
    return totalActiveSellers;
  }

  public void setTotalActiveSellers(int totalActiveSellers) {
    this.totalActiveSellers = totalActiveSellers;
  }

  public ContextData getContextData() {
    return contextData;
  }

  public void setContextData(ContextData contextData) {
    this.contextData = contextData;
  }

  public boolean isValid(){
    return totalActiveQuantity > 5 || totalActiveSellers > 3;
  }
}
