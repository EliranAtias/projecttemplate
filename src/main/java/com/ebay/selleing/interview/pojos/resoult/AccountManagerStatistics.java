package com.ebay.selleing.interview.pojos.resoult;

import com.ebay.selleing.interview.pojos.mocks.Condition;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountManagerStatistics{
  private String fullName;

  private Map<Condition, List<ConditionSummary>> conditionSummaryMap;

  public AccountManagerStatistics(){
    conditionSummaryMap = new HashMap<Condition, List<ConditionSummary>>();
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public Map<Condition, List<ConditionSummary>> getConditionSummaryMap() {
    return conditionSummaryMap;
  }

  public void setConditionSummaryMap(Map<Condition, List<ConditionSummary>> conditionSummaryMap) {
    this.conditionSummaryMap = conditionSummaryMap;
  }
}
