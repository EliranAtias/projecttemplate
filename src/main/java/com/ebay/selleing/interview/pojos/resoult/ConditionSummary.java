package com.ebay.selleing.interview.pojos.resoult;

import com.ebay.selleing.interview.pojos.mocks.Product;

public class ConditionSummary{
  private String epid;
  private Boolean contextAsSite;
  private Boolean contextAsSeller;

  public ConditionSummary(Product product){
    epid = product.getEpid();
    contextAsSite = product.getContextData().getContextAsSite();
    contextAsSeller = product.getContextData().getContextAsSeller();
  }

  public String getEpid() {
    return epid;
  }

  public void setEpid(String epid) {
    this.epid = epid;
  }

  public Boolean getContextAsSite() {
    return contextAsSite;
  }

  public void setContextAsSite(Boolean contextAsSite) {
    this.contextAsSite = contextAsSite;
  }

  public Boolean getContextAsSeller() {
    return contextAsSeller;
  }

  public void setContextAsSeller(Boolean contextAsSeller) {
    this.contextAsSeller = contextAsSeller;
  }
}
