package com.ebay.selleing.interview.services;

import com.ebay.selleing.interview.pojos.mocks.Condition;
import com.ebay.selleing.interview.pojos.mocks.Document;
import com.ebay.selleing.interview.pojos.resoult.AccountManagerStatistics;
import com.ebay.selleing.interview.pojos.resoult.ConditionSummary;
import com.ebay.selleing.interview.utils.MockReader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AppService{

  private Map<Condition,List<Document>> mockSourceData = new HashMap<>();

  @PostConstruct
  public void init(){
    try{
      mockSourceData.put(Condition.NEW, MockReader.loadListMockResponse("mocks/DocsConditionNew.json"));
      mockSourceData.put(Condition.USED, MockReader.loadListMockResponse("mocks/DocsConditionUsed.json"));
    }catch(Exception e){
      e.printStackTrace();
    }
    System.out.println("Init success");
  }

  public AccountManagerStatistics getAccountManagerStatistics(String accountManager){
    Map<Condition, List<ConditionSummary>> result = generateConditionSummaryMap(accountManager);
    AccountManagerStatistics accountManagerStatistics = new AccountManagerStatistics();
    accountManagerStatistics.setConditionSummaryMap(result);
    accountManagerStatistics.setFullName(accountManager);
    return accountManagerStatistics;
  }

  private Map<Condition, List<ConditionSummary>> generateConditionSummaryMap(String accountManager){
    Map<Condition, List<ConditionSummary>> result = new HashMap<Condition, List<ConditionSummary>>();
    mockSourceData.entrySet().stream().forEach(entry -> {
      List<ConditionSummary> currConditionSummaries = entry.getValue().stream().filter(document -> {
        return document.getProduct().isValid() && document.getItems().stream().anyMatch(item -> item.getAccountManager() != null && item.getAccountManager().equals(accountManager));
      }).map(document -> new ConditionSummary(document.getProduct())).collect(Collectors.toList());
      result.put(entry.getKey(), currConditionSummaries);
    });

    return result;
  }

}